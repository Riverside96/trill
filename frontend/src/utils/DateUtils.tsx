// const DAYS: string []=""
const MONTHS: string[] = [
  '',
  'January',
  'Febuary',
  'March',
  'April',
  'May',
  'June',
  'July',
  'August',
  'September',
  'October',
  'November',
  'December'
];

export const getDays = ():JSX.Element[] => {
  let options:JSX.Element[] = [];
  for(let i=0; i<32; i++){
    options.push( i===0 ? 
      <option value={0} key={i}></option> : 
      <option value={i} key={i}>{i}</option>
    )
  }
  return options;
}

export const getMonths = ():JSX.Element[] => {
  return MONTHS.map((month, index) => {
      return <option value={index} key={month}>{index!==0 && month}</option>
  })
}

export const getYears = ():JSX.Element[] => {
  let options:JSX.Element[] = [];
  for(let i=2023; i>1900; i--){
    options.push( i===2023 ?
      <option value={0} key={i}></option> :
      <option value={i} key={i}>{i}</option>
    )
  }
  return options;
}



// export const getMonths = ():JSX.Element[] => {
//   return MONTHS.map((month, index) => {
//     if(index === 0){
//       return <option value={index} key={month}></option>
//     }
//     else{
//       return <option value={index} key={month}>{month}</option>
//     }
//   })
// }

