import { StyledInputProps, ValidatedInputState } from "./GlobalInterfaces";

export const determineStyledInputBorder = (props:StyledInputProps):string => {
  let {active, valid, theme} = props
  
  if(!active && valid){
    return `1px solid ${theme.colors.lightgrey}`
  }
  if(!active && !valid){
    return `1px solid ${theme.colors.error}`
  }
  if(active && valid){
    return `2px solid ${theme.colors.blue}`
  }
  if(active && !valid){
    return `2px solid ${theme.colors.error}`
  }
  return "";
}

export const determineLabelColor = (props:StyledInputProps):string => {
  let {active, valid, theme, color} = props;

  if(color && color==='error'){               //color is optional so check it is defined before further op
    return theme.colors.error;
  }
  if(color && color==='blue')
    return theme.colors.blue

  return theme.colors.grey
  
}

export const determineValidatedStyles = (state:ValidatedInputState, validator:(value:string)=>boolean): ValidatedInputState => {
  let {valid, active, typedIn, val, labelColor, labelActive} = state;
  
  if(typedIn){
    valid = validator(val);
    
    if(active && valid){
      labelActive=true;
      labelColor='blue';
    }
    if(active && !valid){
      labelActive=true;
      labelColor='error';
    }
    if(!active && valid){
      labelActive=true;
      labelColor='grey'
    }
    if(!active && !valid){
      labelActive=false;
      labelColor='grey'
    }
  }else{
    if(active){
      labelActive=true
      labelColor='blue'
    }else{
      labelActive=false;
      labelColor='grey'
    }
  }

  state = {
    ...state,
    valid,
    labelActive,
    labelColor
  }

  return state;
}

export const determineValidatedSelectStyle = (active:boolean, valid:boolean): string => {
  if(!valid) return "error"
  if(active) return "blue"
  else       return "grey"
}
