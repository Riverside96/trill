import React from 'react'
import './App.css'
import './assets/global.css'
import Landing from './pages/Landing/Landing.tsx'
import { ThemeProvider, createGlobalStyle } from 'styled-components'
import { Theme } from './utils/GlobalInterfaces.tsx'

const theme:Theme = {
  colors: {
    blue: '#1DA1F2',
    black: '#14171a',
    grey: '#657786',
    lightgrey: '#AAB8ED',
    offwhite: '#E1E8ED',
    white: '#F5F8FA',
    error: 'red'
  }
}

const GlobalStyle = createGlobalStyle`
  *{
    font-family: 'IBM Plex Sans', sans-serif,
    font-weight: 500;
  }
`


const App:React.FC= () => {
  return (
    <ThemeProvider theme={theme}>
      <Landing />
    </ThemeProvider>
  )
}

export default App  
