import React from 'react'
import './Landing.css'
import RegisterModal from '../../features/register/components/RegisterModal/RegisterModal'

const Landing:React.FC = () => {
  return (
    <div className='home-container bg-color'>
      <RegisterModal />
    </div>
  )
}

export default Landing 


