import React, { useEffect, useState } from 'react'
import './RegistrationForm1.css'
import {TextInput} from '../../../../../components/TextInput.tsx'
import { ValidatedInput } from '../../../../../components/ValidatedInput/ValidatedInput.tsx'
import { validateName } from '../../../../../services/Validators.ts'
import { RegisterDateInput } from '../../RegisterDateInput/RegisterDateInput.tsx'

interface FormOneState {
  firstName: string
  lastName: string
  email: string
  dob: string
}

export const RegistrationForm1:React.FC = () => {
  const [stepOneState, setStepOneState] = useState({
    firstName: "",
    lastName: "",
    email: "",
    dob: ""
  })

  function updateUser(e:React.ChangeEvent<HTMLInputElement>):void {
    setStepOneState({...stepOneState, [e.target.name]: e.target.value})
  }

  useEffect(() => {
    console.log("state changed", stepOneState)
  }, [stepOneState])

  return (
    <div className='reg-step-one-container'>
      <div className='reg-step-one-content'>
        <ValidatedInput 
          name={"firstName"}
          label={"First"}
          errorMsg={"What's your name?"}
          changeVal={updateUser}
          validator={validateName}
        />
        <ValidatedInput 
          name={"lastName"}
          label={"Last"}
          errorMsg={"What's your name?"}
          changeVal={updateUser}
          validator={validateName}
        />
        <ValidatedInput 
          name={"email"}
          label={"Email"}
          errorMsg={"Please enter a valid email"}
          changeVal={updateUser}
          validator={() => true}
        />
        <RegisterDateInput />
     

    

      </div>
    </div>
  )
}

