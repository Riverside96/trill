import React, {useState} from 'react'
import Modal from '../../../../components/Modal/Modal.tsx'
import './RegisterModal.css'
import StepCount from '../StepCounter/StepCount.tsx'
import {determineModalContent} from './RegisterModalUtils.tsx'

const RegisterModals:React.FC = () => {
  const [step, setStep] = useState<number>(1);
  
  const handleStepBtnClick =()=> setStep(step => (
    step==1  || step===4 || step >= 6 ? 
      step :
      step-1)
  );
  

  return (
    <Modal>
      <div className='register-modal'>
        <StepCount step={step} changeStep={handleStepBtnClick} />
        <div className='register-modal-content'>
          {determineModalContent(step)}
        </div>
      </div>
    </Modal>
  )
}

export default RegisterModals
