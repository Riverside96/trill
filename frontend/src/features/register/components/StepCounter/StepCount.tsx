import React from 'react'
import './StepCount.css'
import {displayIcon} from './StepCountUtils.tsx'

interface StepProps{
  step: number;
  changeStep():void
}

function showBtnClass(step:number):string  {
  return (step===4 || step===6) ? 
    "reg-step-count-btn-disable":
    "reg-step-count-btn";
}


export const StepCount:React.FC<StepProps> = ({step, changeStep}) => {
  return (
    <div className='reg-step-count-container'>
      <div className={showBtnClass(step)} onClick={changeStep}>
        {displayIcon(step)}
      </div>
      <span className='reg-step-num'>Step {step} of 6 </span>

    </div>
  )
}
export default StepCount



