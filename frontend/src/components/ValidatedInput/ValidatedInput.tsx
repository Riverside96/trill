import React, { useEffect, useState } from 'react'
import { StyledInputBox, StyledInputLabel } from './StyledInput'
import { ValidatedInputState } from '../../utils/GlobalInterfaces';
import './ValidatedInput.css'
import { determineValidatedStyles } from '../../utils/DetermineStylesUtils';

interface ValidatedUserInputProps {
  name:string;
  label:string;
  errorMsg: string;
  validator(value:string):boolean;
  changeVal(e:React.ChangeEvent<HTMLInputElement>):void;
  attributes?:Record<string, string | number | boolean>;

}

export const ValidatedInput:React.FC<ValidatedUserInputProps> = ({name, label, errorMsg, validator, changeVal, attributes}) => {
  
  const[validatedState, setValidatedState] = useState<ValidatedInputState>({
    active: false,
    valid: true,
    typedIn: false,
    labelActive: false,
    labelColor: "grey",
    val: ''
  });

  useEffect(()=> {
    setValidatedState(determineValidatedStyles(validatedState, validator))
  },[validatedState.active, validatedState.typedIn, validatedState.val, validatedState.labelActive, validatedState.labelColor])

  const focus = (e:React.FocusEvent<HTMLInputElement>):void => {
    setValidatedState({
      ...validatedState,
      active: !validatedState?.active
    })
  }
  const updateVal = (e:React.ChangeEvent<HTMLInputElement>):void => {
    setValidatedState({
      ...validatedState,
      typedIn: true,
      val: e.target.value
    })
    changeVal(e);
  }



  return (
    <div className='validated-input'>
      <StyledInputBox 

        active={validatedState.active}
        valid={validatedState.valid}>
        
        <StyledInputLabel 
          color={validatedState.labelColor}
          active={validatedState.labelActive}
          valid={validatedState.valid}> 
          {label} 
        </StyledInputLabel>
        
        <input className='validated-input-val'
          onFocus={focus}
          onBlur={focus}
          onChange={updateVal}
          {...attributes}
        />
      </StyledInputBox>

      {!validatedState.valid && <span>{errorMsg}</span>}
    </div>
  )
}
