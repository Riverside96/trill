import React, { useEffect, useState } from 'react'
import { StyledInputBox, StyledInputLabel } from './StyledInput'
import { determineValidatedSelectStyle } from '../../utils/DetermineStylesUtils'

interface ValidateDateSelectorProps {
  style:string
  valid:boolean
  name:string
  dropdown():JSX.Element[]

}

export const ValidatedDateSelector:React.FC<ValidateDateSelectorProps> = ({style, valid, name, dropdown}) => {
  
  const [active, setActive] = useState<boolean>(false)
  const [value, setValue] = useState<number>(0)
  const [color, setColor] = useState<string>('grey')

  const changeValue = (e:React.ChangeEvent<HTMLSelectElement>) => {
    console.log("dispatch this change to a reducer")
    console.log(e.target.value)
    setValue(+e.target.value)       //changes val to number
  }

  const toggleActive = (e:React.FocusEvent<HTMLSelectElement>) => {
    setActive(!active);
  }

  useEffect(() => {
    setColor(determineValidatedSelectStyle(active, valid))
  },[active, valid, value])

  return (
    <div className={style}>
      <StyledInputBox active={active} valid={valid}>
        <StyledInputLabel color='grey' active={true} valid={valid}>
          {name}
        </StyledInputLabel>
        <select onChange={changeValue} onFocus={toggleActive} onBlur={toggleActive}>
          {dropdown()}
        </select>
      </StyledInputBox>
    </div>
  )
}
