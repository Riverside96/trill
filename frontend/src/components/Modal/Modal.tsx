import React from 'react'
import './Modal.css'

interface ModalProps {
  children: React.ReactNode
}

const Modal:React.FC<ModalProps> = (props:ModalProps) => {
  return (
    <div className='modal-overlay'>
      <div className='modal-container bg-color'>
        {props.children}
      </div>
    </div>
  )
}

export default Modal
