import React, { useState } from 'react'

interface TextInputProps{
  name: string
  label: string
  error: string
  onChange(event:React.ChangeEvent<HTMLInputElement>):void  // e for event
  maxLength?:number
  validator?(value:string):boolean
}

export const TextInput:React.FC<TextInputProps> = ({name, label, error, onChange, maxLength, validator}) => {

  const [inputVal, setInputVal] = useState<String>("")

  function updateInput(event:React.ChangeEvent<HTMLInputElement>) { 
    onChange(event)                 //so we can pass props backwards?
    setInputVal(event.target.value)

  }

  return (
    <div className='text-input'>
      <div>
        <span>{label}</span>
        <input name={name} onChange={updateInput} />

      </div>

    </div>
  )
}
