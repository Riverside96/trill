package com.riverside96.exceptions;

public class UserDoesNotExistException extends RuntimeException {
  
  private static final long serialVersionUID = 1L;
  
  public UserDoesNotExistException() {
    super("It looks like that user doesn't live here");

  
  }
}
