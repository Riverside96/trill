package com.riverside96.exceptions;

public class IncorrectVerificationCodeException extends RuntimeException {
  private static final long serialVersionUID = 1L;

  public IncorrectVerificationCodeException() {
    super("The verification code entered appears to be incorrect!");
  }

}
