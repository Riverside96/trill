package com.riverside96.exceptions;

public class EmailAlreadyTakenException extends RuntimeException {
  
  // needed to extend RuntimeException
  private static final long serialVersionUID = 1L;

  public EmailAlreadyTakenException(){
     super("This email is already taken");
  }
  
}
