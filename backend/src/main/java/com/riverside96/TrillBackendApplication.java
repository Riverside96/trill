package com.riverside96;


import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import com.riverside96.models.AppUser;
import com.riverside96.models.Role;
import com.riverside96.repos.RoleRepo;
import com.riverside96.services.UserService;

@SpringBootApplication
public class TrillBackendApplication {

	public static void main(String[] args) {
		SpringApplication.run(TrillBackendApplication.class, args);
	}

  @Bean
  CommandLineRunner run(RoleRepo roleRepo, UserService userService){
    return args -> {
      roleRepo.save(new Role(1, "USER"));         
      AppUser user = new AppUser();
      user.setfName("Rory");
      user.setlName("Boyes");
      // userService.registerUser(user);
    };
  }

}
