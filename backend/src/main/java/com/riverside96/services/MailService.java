package com.riverside96.services;

import java.io.ByteArrayOutputStream;
import java.util.Properties;

import javax.mail.Session;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.api.services.gmail.Gmail;
import com.google.api.services.gmail.model.Message;
import com.riverside96.exceptions.EmailFailedToSendException;

@Service
public class MailService {
  
  private final Gmail gmail;

  @Autowired
  public MailService(Gmail gmail) {
    this.gmail = gmail;
  }

  public void sendEmail(String toAddress, String subject, String content) throws Exception {
    Properties props = new Properties();
    Session session = Session.getInstance(props, null);
    MimeMessage email = new MimeMessage(session);
    try {
      email.setFrom(new InternetAddress("roryboyes96@gmail.com"));
      email.addRecipient(javax.mail.Message.RecipientType.TO, new InternetAddress(toAddress));
      email.setSubject(subject);
      email.setText(content);
      ByteArrayOutputStream buffer = new ByteArrayOutputStream();
      email.writeTo(buffer);
      byte[] rawMsgBytes = buffer.toByteArray();
      String encodedMail = Base64.encodeBase64URLSafeString(rawMsgBytes);
      Message msg = new Message();
      msg.setRaw(encodedMail);
      msg = gmail.users().messages().send("me", msg).execute();
    } catch (Exception e){
        throw new EmailFailedToSendException();
    }

  }

}
