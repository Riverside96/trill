package com.riverside96.services;

import java.util.Set;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.riverside96.exceptions.EmailAlreadyTakenException;
import com.riverside96.exceptions.EmailFailedToSendException;
import com.riverside96.exceptions.IncorrectVerificationCodeException;
import com.riverside96.exceptions.UserDoesNotExistException;
import com.riverside96.models.AppUser;
import com.riverside96.models.RegistrationObject;
import com.riverside96.models.Role;
import com.riverside96.repos.RoleRepo;
import com.riverside96.repos.UserRepo;
import com.riverside96.services.MailService;

@Service
public class UserService {
  private final UserRepo userRepo;
  private final RoleRepo roleRepo;
  private final MailService mailService;
  private final PasswordEncoder passwordEncoder;


  @Autowired
  public UserService(UserRepo userRepo, RoleRepo roleRepo, MailService mailService, PasswordEncoder passwordEncoder){
      this.userRepo = userRepo;
      this.roleRepo = roleRepo;
      this.mailService = mailService;
      this.passwordEncoder = passwordEncoder;
    }

  public AppUser registerUser(RegistrationObject regObj){
    AppUser user = new AppUser();
    user.setfName(regObj.getfName());
    user.setlName(regObj.getlName());
    user.setEmail(regObj.getEmail());
    user.setDateOfBirth(regObj.getDob());

    // setting handle with random num & collision check
    String name = user.getfName() + user.getlName();
    boolean nameTaken = true;
    String tempName = "";
    while(nameTaken){
      tempName = generateUsername(name);
      if(userRepo.findByUsername(tempName).isEmpty()){
        nameTaken = false;
      }
    }
    user.setUsername(tempName);

    Set<Role> roles = user.getAuthorities();
    roles.add(roleRepo.findRoleByAuthority("USER").get());        // get as its an optional
    user.setAuthorities(roles);

    try{
    return userRepo.save(user);
    }catch (Exception e){
      throw new EmailAlreadyTakenException();
    }
  }

  private String generateUsername(String name){
    // generate a 9 digit number
    long autoNum = (long)Math.floor(Math.random() * 1_000_000_000);
    return name+autoNum;
  }

  public void generateEmailVerification(String username) {
    AppUser user = userRepo.findByUsername(username).orElseThrow(UserDoesNotExistException::new);
    user.setVerification(generateVerificationNumber());

    try {
      mailService.sendEmail(user.getEmail(), "Verfication Code", 
        "Here is your Verification code to get you up & running with Trill: " + user.getVerification());
        userRepo.save(user);
    } catch (Exception e) {
      throw new EmailFailedToSendException();
    }     

    userRepo.save(user);
  }

  private Long generateVerificationNumber() {
    return (long)Math.floor(Math.random() * 1_000_000);
  }

  public AppUser getUserByUsername(String username) {
      return userRepo.findByUsername(username).orElseThrow(UserDoesNotExistException::new);
  }

  public AppUser updateUser(AppUser user) {
      try {
        return userRepo.save(user);
      } catch(Exception e){
        throw new EmailAlreadyTakenException();
      }
  }

  public AppUser verifyEmail(String username, Long verificationCode) {
      AppUser user = userRepo.findByUsername(username).orElseThrow(UserDoesNotExistException::new);
      if(verificationCode.equals(user.getVerification())) {
        user.setEnabled(true);
        user.setVerification(null);
        return userRepo.save(user);      
    }else{
      throw new IncorrectVerificationCodeException();
    }
  }

public AppUser setPassword(String username, String password) {
    AppUser user = userRepo.findByUsername(username).orElseThrow(UserDoesNotExistException::new);
    String encodedPass = passwordEncoder.encode(password);
    user.setPassword(encodedPass);
    return userRepo.save(user);
}


}
