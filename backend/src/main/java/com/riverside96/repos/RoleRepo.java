package com.riverside96.repos;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.riverside96.models.Role;

@Repository
public interface RoleRepo extends JpaRepository<Role, Integer>{
  
  Optional<Role> findRoleByAuthority(String authority);


}
