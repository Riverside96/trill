package com.riverside96.repos;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.riverside96.models.AppUser;

@Repository
public interface UserRepo extends JpaRepository<AppUser, Integer>{

  // optional to avoid passing nulls around
  Optional<AppUser> findByUsername(String username);


}
