package com.riverside96.controllers;

import java.util.LinkedHashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.riverside96.exceptions.EmailAlreadyTakenException;
import com.riverside96.exceptions.EmailFailedToSendException;
import com.riverside96.exceptions.IncorrectVerificationCodeException;
import com.riverside96.exceptions.UserDoesNotExistException;
import com.riverside96.models.AppUser;
import com.riverside96.models.RegistrationObject;
import com.riverside96.services.UserService;

@RestController
@RequestMapping("/auth")
public class AuthController {
  private final UserService userService;

  @Autowired
  public AuthController(UserService userService){
      this.userService = userService;
    }
  
  @ExceptionHandler({EmailAlreadyTakenException.class})
  public ResponseEntity<String> handleEmailTaken(){
    return new ResponseEntity<String>("This email is already taken", HttpStatus.CONFLICT);
  }

  // user doesnt exist handler 
  @ExceptionHandler({UserDoesNotExistException.class})
  public ResponseEntity<String> handleUserDoesNotExist(){
    return new ResponseEntity<String>("The user you're looking for doesn't live here yet", HttpStatus.NOT_FOUND);
  }

  // always return the object in question from a put or post so that the front end can verify the change
  // extends RequestMapping path, ie, auth/register
  @PostMapping("/register")
  public AppUser registerUser(@RequestBody RegistrationObject regObj){
    return userService.registerUser(regObj);
  }

  @PutMapping("/update/phone")
  public AppUser updatePhoneNumber(@RequestBody LinkedHashMap<String, String> body){
    String username = body.get("username");
    String number = body.get("phone");
    AppUser user = userService.getUserByUsername(username);
    user.setPhone(number);
    return userService.updateUser(user);
  }

  @ExceptionHandler({EmailFailedToSendException.class})
  public ResponseEntity<String> handleFailedEmail(){
    return new ResponseEntity<String>(
    "Email failed to send, try again shortly", HttpStatus.OK);
  }

  @PostMapping("/email/code")
  public ResponseEntity<String> createEmailVerification(@RequestBody LinkedHashMap<String, String> body) {
    userService.generateEmailVerification(body.get("username"));
    return new ResponseEntity<String>("Verification email has been sent", HttpStatus.OK);
  }

  @PostMapping("/email/verify")
  public AppUser verifyEmail(@RequestBody LinkedHashMap<String, String> body){
    Long verificationCode = Long.parseLong(body.get("code"));
    String username = body.get("username");
    return userService.verifyEmail(username, verificationCode);
  }

  @ExceptionHandler({IncorrectVerificationCodeException.class})
  public ResponseEntity<String> handleIncorrectVerificationCode(){
    return new ResponseEntity<String>("The verification code entered appears to be incorrect!", HttpStatus.CONFLICT);
  }

  @PutMapping("/update/password")
  public AppUser updatePassword(@RequestBody LinkedHashMap<String, String> body){
    String username = body.get("username");
    String password = body.get("password");
    return userService.setPassword(username, password);
  }

}
