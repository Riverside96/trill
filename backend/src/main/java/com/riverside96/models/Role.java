package com.riverside96.models;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

@Entity                                                 // tell JPA we want to store in db & hydrate
@Table(name="roles")                                    // override default table name
public class Role {
  @Id                                                   // begin defining primary key
  @GeneratedValue(strategy=GenerationType.AUTO)         // define how we will generate the keys
  @Column(name="role_id")                               // define primary key name
  private Integer roleID;                               // define primary key type
  
  private String authority;                             // for spring security

  public Role(){
    super();
  }

  public Role(Integer roleID, String authority){
    super();
    this.roleID = roleID;
    this.authority = authority;
  }


  public Integer getRoleID() {
    return roleID;
  }

  public void setRoleID(Integer roleID) {
    this.roleID = roleID;
  }

  public String getAuthority() {
    return authority;
  }

  public void setAuthority(String authority) {
    this.authority = authority;
  }

  @Override
  public String toString() {
    return "Role [authority=" + authority + ", roleID=" + roleID + "]";
  }


}
