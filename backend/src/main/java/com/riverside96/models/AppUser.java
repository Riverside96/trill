package com.riverside96.models;

import java.sql.Date;
import java.util.HashSet;
import java.util.Set;

import com.fasterxml.jackson.annotation.JsonIgnore;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.JoinTable;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.Table;


@Entity
@Table(name="users")
public class AppUser {
  @Id
  @GeneratedValue(strategy=GenerationType.AUTO)
  @Column(name="user_id")
  private Integer userID;
  
  @Column(name="first_name")
  private String fName;
  
  @Column(name="last_name")
  private String lName;

  @Column(unique=true)
  private String email;

  private String phone;                 //multiple account may have the same number
  
  @Column(name="dob")
  private Date dateOfBirth;

  @Column(unique=true)
  private String username;
  
  @JsonIgnore                           // we dont want this inside the user object that we pass around (security)
  private String password;

  @ManyToMany(fetch=FetchType.EAGER)    // we always fetch the users roles EVERY TIME we fetch the user
  @JoinTable(
    name="user_role_junction",          // naming convention (preference)
    joinColumns = {@JoinColumn(name="user_id")},
    inverseJoinColumns = {@JoinColumn(name="role_id")}
  )
  private Set<Role> authorities;

  private Boolean enabled;

  @Column(nullable=true)
  @JsonIgnore                           // we dont want the user to see the verification code
  private Long verification;


  //===================================Methods==================================================================//
  public AppUser(){
    this.authorities = new HashSet<>(); // avoid checking if its null (null error)
    this.enabled = false;               // stop user using account until completion
  }

  public Integer getUserID() {
    return userID;
  }

  public void setUserID(Integer userID) {
    this.userID = userID;
  }

  public String getfName() {
    return fName;
  }

  public void setfName(String fName) {
    this.fName = fName;
  }

  public String getlName() {
    return lName;
  }

  public void setlName(String lName) {
    this.lName = lName;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getPhone() {
    return phone;
  }

  public void setPhone(String phone) {
    this.phone = phone;
  }

  public Date getDateOfBirth() {
    return dateOfBirth;
  }

  public void setDateOfBirth(Date dateOfBirth) {
    this.dateOfBirth = dateOfBirth;
  }

  public String getUsername() {
    return username;
  }

  public void setUsername(String username) {
    this.username = username;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public Set<Role> getAuthorities() {
    return authorities;
  }

  public void setAuthorities(Set<Role> authorities) {
    this.authorities = authorities;
  }

  public Boolean getEnabled() {
    return enabled;
  }

  public void setEnabled(Boolean enabled) {
    this.enabled = enabled;
  }

  public Long getVerification() {
    return verification;
  }

  public void setVerification(Long verification) {
    this.verification = verification;
  }

  @Override
  public String toString() {
    return "AppUser [userID=" + userID + ", fName=" + fName + ", lName=" + lName + ", email=" + email + ", phone="
        + phone + ", dateOfBirth=" + dateOfBirth + ", username=" + username + ", password=" + password
        + ", authorities=" + authorities + ", enabled=" + enabled + ", verification=" + verification + "]";
  }

  

  //============================================================================================================//
  

}


// note, name mapping using Column notation is not necassary but allows us to follow naming conventions of java & sql
