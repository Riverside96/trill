package com.riverside96.models;

import java.sql.Date;


// a regObj is a user with only the nec props for reg. This avoids creating a custom constructor? as we might want to protect against creating a user with only these props elsewhere?
public class RegistrationObject {
  private String fName;
  private String lName;
  private String email;
  private Date dob;
public String getfName() {
	return fName;
}
public void setfName(String fName) {
	this.fName = fName;
}
public String getlName() {
	return lName;
}
public void setlName(String lName) {
	this.lName = lName;
}
public String getEmail() {
	return email;
}
public void setEmail(String email) {
	this.email = email;
}
public Date getDob() {
	return dob;
}
public void setDob(Date dob) {
	this.dob = dob;
}
@Override
public String toString() {
  return "RegistrationObject [fName=" + fName + ", lName=" + lName + ", email=" + email + ", dob=" + dob + "]";
}
}
